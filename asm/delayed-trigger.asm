.include "tn84def.inc"


.org 0x000
	rjmp main

.org OC1Aaddr
  rjmp INTtimer

;.org ACIaddr debug
  ;rjmp INTac

; Registermap:

.def delayTimeMSB = r16
.def delayTimeLSB = r17
; delayTimeMSB,delayTimeLSB ; die 16 Bit der delayzeit

.def holdOffTimeMSB = r18
.def holdOffTimeLSB = r19
; holdOffTimeMSB,holdOffTimeLSB ; die 10 Bit der holdoffzeit (right orientated). (holdOffTimeLSB entspricht nachher dem MSB des Timers und die beiden bits in holdOffTimeMSB werden als Zähler für Timerdurchläufe genutzt)

.def holdOffTimeMSBworking = r20
; holdOffTimeMSBworking ; die heruntergezählten 2 MSBs der HolfOffTime
;ausserdem zeigt das MSB des Registers an, ob gerade der HoldoffTiemr aktiv ist
;( highPersMode ; zeigt an ob highPersMode an ist)
;

.def ADCnextPort = r22
; hier wird gespeichert welchen Pin die ADC gerade gemessen hat

.def ACintDisabled = r23

.def TimerTCCR1BstartTimer = r24

.def RegZero = r0

.def PortTmpWriteReg = r25
.def PortTmpWriteRegInt = r21

.def testReg = r1


; PIN defs:

; PORTA
.equ TimeFeinPin = 0
.equ TriggerInputPin = 1
.equ TriggerLevelPin = 2
.equ TimeGrobPin = 3
.equ TriggerOutPin = 6
.equ HoldOffPin = 7

; PORTB
.equ ModePin = 2

; ADCin pins:
.equ TimeFeinADMUX = 0x00
.equ TimeGrobADMUX = 0b00000011
.equ HoldOffADMUX =  0b00000111

.equ ADCnextBitBegin = 0
.equ ADCnextBitTimeFein = 1
.equ ADCnextBitTimeGrob = 2
.equ ADCnextBitHoldOff = 3

.equ ADCSRB_ADLAR_RIGHT = 0x00
.equ ADCSRB_ADLAR_LEFT = (1<<ADLAR)

main:

    clr RegZero

    ;ports initialiesieren
    ldi PortTmpWriteReg,(1<<TriggerOutPin)
    out DDRA,PortTmpWriteReg

    ; PORTB ist alles INPUT
    ; ModePin ohne PullUp, denn es wird nur zwischen Demo und Normal unterschieden


    ; timerprescaler einstellen
    ; Vorerst zum Ausprobieren clkIO/256
    ;ldi TimerTCCR1BstartTimer,(1<<CS12)
    ;ldi TimerTCCR1BstartTimer,(1<<CS11)|(1<<CS10)
		ldi TimerTCCR1BstartTimer,(1<<CS10)

    ; DEBUG
    ldi PortTmpWriteRegInt,(1<<OCIE1A) ;int aktivieren
    out TIMSK1,PortTmpWriteRegInt

    out TCCR1B,TimerTCCR1BstartTimer ;timer starten
    ; DEBUG



    ; global interrupts aktivieren
    in PortTmpWriteReg,SREG
    ori PortTmpWriteReg,(1<<SREG_I)
    out SREG,PortTmpWriteReg

    ; AC aktivieren, INT dafür setzen
    ; auf rising edge

    ldi ACintDisabled,(1<<ACIS0)|(1<<ACIS1)
    ; in Register merken um sofort zu nutzen

    ldi PortTmpWriteReg,(1<<ACIE)|(1<<ACIS0)|(1<<ACIS1)
    ;out ACSR, PortTmpWriteReg debug

    ; ADC anschalten
    ldi PortTmpWriteReg,(1<<ADEN)
    out ADCSRA,PortTmpWriteReg

    ldi ADCnextPort,(1<<ADCnextBitBegin)





ADCloop:
    ; nachster ADC Pin aussuchen

    sbrc ADCnextPort,ADCnextBitHoldOff ;wenn bei HolfOff angekommen, neu anfangen
    ldi ADCnextPort,(1<<ADCnextBitBegin)

    lsl ADCnextPort

    sbrc ADCnextPort,ADCnextBitTimeGrob
    ldi PortTmpWriteReg,TimeGrobADMUX

    sbrc ADCnextPort,ADCnextBitTimeFein
    ldi PortTmpWriteReg,TimeFeinADMUX

    sbrc ADCnextPort,ADCnextBitHoldOff
    ldi PortTmpWriteReg,HoldOffADMUX

    out ADMUX,PortTmpWriteReg

    ; bei HoldOffTime Ergebniss rechts orientieren, sonst links
    sbrc ADCnextPort,ADCnextBitHoldOff
    ldi PortTmpWriteReg,ADCSRB_ADLAR_RIGHT
    sbrs ADCnextPort,ADCnextBitHoldOff
    ldi PortTmpWriteReg,ADCSRB_ADLAR_LEFT

    out ADCSRB,PortTmpWriteReg


    ldi PortTmpWriteReg,(1<<ADEN)|(1<<ADSC)
    out ADCSRA, PortTmpWriteReg

    ; ADC starten
ADCwaitLoop:
    sbic ADCSRA,ADSC
    rjmp ADCwaitLoop
    ; solange noch nicht fertig rjmp ADCwaitLoop

    sbrc ADCnextPort,ADCnextBitTimeGrob
    rjmp ADCdelayTimeMSB


    sbrc ADCnextPort,ADCnextBitTimeFein
    rjmp ADCdelayTimeLSB


    ; dann ist wohl HolfOff abgefragt worden
    in  holdOffTimeLSB, ADCL
    in  holdOffTimeMSB, ADCH

    rjmp ADCloop

ADCdelayTimeMSB:
		in r26,ADCL
    in delayTimeMSB, ADCH
    rjmp ADCloop

ADCdelayTimeLSB:
		in r26,ADCL
    in delayTimeLSB, ADCH
    rjmp ADCloop




INTac:
    ; INT AC deaktivieren
    out ACSR, ACintDisabled
    ; wenn nicht demo
    sbis PINB,ModePin
    rjmp INTacEnd


    out TCNT1H,RegZero
    out TCNT1L,RegZero

    out OCR1AH,delayTimeMSB
    out OCR1AL,delayTimeLSB

    ldi PortTmpWriteRegInt,(1<<OCIE1A) ;int aktivieren
    out TIMSK1,PortTmpWriteRegInt

    out TCCR1B,TimerTCCR1BstartTimer ;timer starten

    ; delayTime in Timer kopieren
    ; Timer starten
    ; return aus INT
INTacEnd:
    reti







INTtimer:
    out TCCR1B,RegZero ; timer stoppen

		tst holdOffTimeMSBworking
		brne timerHoldOff ; brancht wenn holdOffTimeMSBworking!=0

		sbis PINB,ModePin ; macht den Pin nicht an, wenn in Demo, er soll dann ca 15 Zeilen tiefer aus gemacht werden
    sbi PORTA,TriggerOutPin

    out TCNT1H,RegZero
    out TCNT1L,RegZero

    out OCR1AH,holdOffTimeLSB
    out OCR1AL,RegZero
		mov holdOffTimeMSBworking,holdOffTimeMSB
		ori holdOffTimeMSBworking,(1<<7)


		cbi PORTA,TriggerOutPin ; Trigger wieder aus machen. Oszi sollte getriggert haben

    ldi PortTmpWriteRegInt,(1<<OCIE1A) ;int aktivieren
    out TIMSK1,PortTmpWriteRegInt

    out TCCR1B,TimerTCCR1BstartTimer ;timer starten

    reti

timerHoldOff:

		cpi holdOffTimeMSBworking,(1<<7)
		breq INTtimerStartAC

		subi holdOffTimeMSBworking,0x01

		out TCNT1H,RegZero ;timer auf null setzen
		out TCNT1L,RegZero ;""

		out OCR1AH,RegZero
    out OCR1AL,RegZero ;timer compare auf 0 stellen, damit er dann komplett durch laeuft und auch wirklich lang genung wartet. Die LSB wurden beim ersten Warten nach der delayTime beachtet


		ldi PortTmpWriteRegInt,(1<<OCIE1A) ;int aktivieren
		out TIMSK1,PortTmpWriteRegInt

		out TCCR1B,TimerTCCR1BstartTimer ;timer starten

		reti
		;
		; return aus INT



INTtimerStartAC: ;aktiviere AC int oder starte Timer in Demo

		mov holdOffTimeMSBworking,RegZero

		sbic PINB,ModePin ; wenn im Demo Mode
		rjmp INTtimerDemo

		;TODO

		reti


    ; wenn holdOffTimeMSBworking > 0: rufe timerHoldOff auf
    ; wenn triggerpin LOW: high machen und Holdoff starten
    ; wenn HIGH, dann LOW machen (TODO, wie lange darf der Trigger aktiv sein?)
        ;timer Stoppen
        ;AC INT aktivieren

INTtimerDemo:

		sbi PORTA,TriggerOutPin ; Trigger an machen um die delayTime zu zeigen

		out TCNT1H,RegZero ;timer null setzen
    out TCNT1L,RegZero

    out OCR1AH,delayTimeMSB
    out OCR1AL,delayTimeLSB

    ldi PortTmpWriteRegInt,(1<<OCIE1A) ;int aktivieren
    out TIMSK1,PortTmpWriteRegInt

    out TCCR1B,TimerTCCR1BstartTimer ;timer starten

		reti
