# Delayed-Trigger

Ein kleines Gerät um einen delayed-trigger bei Oszilloskopen nachzurüsten. Basierend auf einem Attiny84 wird der Trigger verzögert und somit werden später auftretende Signale sichtbar.
